# Front END Technical Test
 

** Welcome to our Front END simple  Technical Test **

** please read the instructions carefully before starting **
Please feel free to spend as much time as you deem necessary and let us know how long you spent in your ** Email ** .

Your goal is to create an APP for the star wars fans : where users can find Star Wars planets list at first  .then by selecting a specific planet they will be able to read more details about it , that it :) [Find the design down below] ,
Don't worry about assets you can use you're own  Make sure you use the Star wars API for the work.[star wars api](https://swapi.co/)
.
There is documentation down Below which describes the endpoint you will need for this App but make sure to check the official documentation [here](https://swapi.co/documentation#planets). 
 
 Please use to create your solution or react to create your solution  .
 

- Send us your code (we recommend using git) 

#Technical questions
** Please answer the following questions in a markdown file called Answers to technical questions.md. **

* How long did you spend on the coding test? 
* What would you add to your solution if you had more time? 
* How would you improve the  app you just made ?

#Api Documentation

###List of endpoints to implement#
- get Planets
``` 
http https://swapi.co/api/planets
```

- Get planet by id   
```
GET /Planet by id 
http https://swapi.co/api/planets/1/
```
 #UI
![Home](https://user-images.githubusercontent.com/17935370/61414627-56941300-a8e6-11e9-80ab-7c68eecfbbbe.jpg)

 ![Planet](https://user-images.githubusercontent.com/17935370/61414623-53992280-a8e6-11e9-9eb4-824e8fb1064f.jpg)



